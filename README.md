# poe-api

`poe-api` is an api library for working with the [path of exile](https://www.pathofexile.com/) [apis](https://www.pathofexile.com/developer/docs/api-resources).

Refer to the [stash API docs](https://pathofexile.gamepedia.com/Public_stash_tab_API).
