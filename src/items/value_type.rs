use std::error::Error;
use std::fmt;
use std::str::FromStr;

use serde_repr::{Deserialize_repr, Serialize_repr};

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct ValueTypeParseError(());

impl fmt::Display for ValueTypeParseError {
    #[allow(deprecated, deprecated_in_future)]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(self.description())
    }
}

impl Error for ValueTypeParseError {
    #[allow(deprecated)]
    fn description(&self) -> &str {
        "unable to parse value type"
    }
}

/// ValueType
#[derive(Serialize_repr, Deserialize_repr, PartialEq, Debug)]
#[repr(u8)]
enum ValueType {
    White,
    Blue,
    Fire,
    Cold,
    Lightning,
    Chaos,
}

impl fmt::Display for ValueType {
    #[allow(deprecated, deprecated_in_future)]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "(ValueType: {})", self.as_str())
    }
}

impl ValueType {
    pub fn as_str(&self) -> &'static str {
        match &self {
            ValueType::White => "white or physical",
            ValueType::Blue => "blue or modified value",
            ValueType::Fire => "fire",
            ValueType::Cold => "cold",
            ValueType::Lightning => "lightning",
            ValueType::Chaos => "chaos",
        }
    }
}

impl FromStr for ValueType {
    type Err = ValueTypeParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let response = match s {
            "white" => Some(ValueType::White),
            "physical" => Some(ValueType::White),
            "magic" => Some(ValueType::Blue),
            "modified" => Some(ValueType::Blue),
            "fire" => Some(ValueType::Fire),
            "cold" => Some(ValueType::Cold),
            "lightning" => Some(ValueType::Lightning),
            "chaos" => Some(ValueType::Chaos),
            _ => None,
        };

        match response {
            Some(response) => Ok(response),
            None => Err(ValueTypeParseError(())),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn from_str() {
        assert_eq!(ValueType::from_str("white"), Ok(ValueType::White));
        assert_eq!(ValueType::from_str("physical"), Ok(ValueType::White));
        assert_eq!(ValueType::from_str("magic"), Ok(ValueType::Blue));
        assert_eq!(ValueType::from_str("modified"), Ok(ValueType::Blue));
        assert_eq!(ValueType::from_str("fire"), Ok(ValueType::Fire));
        assert_eq!(ValueType::from_str("cold"), Ok(ValueType::Cold));
        assert_eq!(ValueType::from_str("lightning"), Ok(ValueType::Lightning));
        assert_eq!(ValueType::from_str("chaos"), Ok(ValueType::Chaos));
        assert_eq!(
            ValueType::from_str("notARealValueType"),
            Err(ValueTypeParseError(()))
        );
    }

    #[test]
    fn as_str() {
        assert_eq!(ValueType::White.as_str(), "white or physical");
        assert_eq!(ValueType::Blue.as_str(), "blue or modified value");
        assert_eq!(ValueType::Fire.as_str(), "fire");
        assert_eq!(ValueType::Cold.as_str(), "cold");
        assert_eq!(ValueType::Lightning.as_str(), "lightning");
        assert_eq!(ValueType::Chaos.as_str(), "chaos");
    }

    #[test]
    fn serialization() -> serde_json::Result<()> {
        let v = serde_json::to_string(&ValueType::Cold)?;
        assert_eq!(v, "3");

        Ok(())
    }
}
