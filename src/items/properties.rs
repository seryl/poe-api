
/**
 * Key	Value description	Type
 * name		string
 * values	array[0] is value, array[1] is valueTypes	array
 * displayMode
 * 0 means name should go before the values.[2]
 * 1 means the values should go after the name.
 * 2 is a progress bar (for skill gem experience).
 * 3 means that the name should have occurances of %1, %2, etc replaced with the values.
 * integer
 * type	properties type	integer
 * progress	additionalProperties's Experience	integer
 */

pub struct Property {
    name: String,
    // values
    // display_mode
    // type
    // progress
}
