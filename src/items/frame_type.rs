use std::error::Error;
use std::fmt;
use std::str::FromStr;

use serde_repr::{Deserialize_repr, Serialize_repr};

/// FrameType
#[derive(Serialize_repr, Deserialize_repr, PartialEq, Debug)]
#[repr(u8)]
pub enum FrameType {
    Normal,
    Magic,
    Rare,
    Unique,
    Gem,
    Currency,
    DivinationCard,
    QuestItem,
    Prophecy,
    Relic,
}

impl fmt::Display for FrameType {
    #[allow(deprecated, deprecated_in_future)]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "(Frame: {})", self.as_str())
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct FrameTypeParseError(());

impl fmt::Display for FrameTypeParseError {
    #[allow(deprecated, deprecated_in_future)]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(self.description())
    }
}

impl Error for FrameTypeParseError {
    #[allow(deprecated)]
    fn description(&self) -> &str {
        "unable to parse frame type"
    }
}

impl FrameType {
    pub fn as_str(&self) -> &'static str {
        match &self {
            FrameType::Normal => "normal",
            FrameType::Magic => "magic",
            FrameType::Rare => "rare",
            FrameType::Unique => "unique",
            FrameType::Gem => "gem",
            FrameType::Currency => "currency",
            FrameType::DivinationCard => "divination card",
            FrameType::QuestItem => "quest item",
            FrameType::Prophecy => "prophecy",
            FrameType::Relic => "relic",
        }
    }
}

impl FromStr for FrameType {
    type Err = FrameTypeParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let response = match s {
            "normal" => Some(FrameType::Normal),
            "magic" => Some(FrameType::Magic),
            "rare" => Some(FrameType::Rare),
            "unique" => Some(FrameType::Unique),
            "gem" => Some(FrameType::Gem),
            "currency" => Some(FrameType::Currency),
            "divination card" => Some(FrameType::DivinationCard),
            "quest item" => Some(FrameType::QuestItem),
            "prophecy" => Some(FrameType::Prophecy),
            "relic" => Some(FrameType::Relic),
            _ => None,
        };

        match response {
            Some(response) => Ok(response),
            None => Err(FrameTypeParseError(())),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use serde_json;

    #[test]
    fn from_str() {
        assert_eq!(FrameType::from_str("normal"), Ok(FrameType::Normal));
        assert_eq!(FrameType::from_str("magic"), Ok(FrameType::Magic));
        assert_eq!(FrameType::from_str("rare"), Ok(FrameType::Rare));
        assert_eq!(FrameType::from_str("unique"), Ok(FrameType::Unique));
        assert_eq!(FrameType::from_str("gem"), Ok(FrameType::Gem));
        assert_eq!(FrameType::from_str("currency"), Ok(FrameType::Currency));
        assert_eq!(
            FrameType::from_str("divination card"),
            Ok(FrameType::DivinationCard)
        );
        assert_eq!(FrameType::from_str("quest item"), Ok(FrameType::QuestItem));
        assert_eq!(FrameType::from_str("prophecy"), Ok(FrameType::Prophecy));
        assert_eq!(FrameType::from_str("relic"), Ok(FrameType::Relic));
        assert_eq!(
            FrameType::from_str("notARealValueType"),
            Err(FrameTypeParseError(()))
        );
    }

    #[test]
    fn as_str() {
        assert_eq!(FrameType::Normal.as_str(), "normal");
        assert_eq!(FrameType::Magic.as_str(), "magic");
        assert_eq!(FrameType::Rare.as_str(), "rare");
        assert_eq!(FrameType::Unique.as_str(), "unique");
        assert_eq!(FrameType::Gem.as_str(), "gem");
        assert_eq!(FrameType::Currency.as_str(), "currency");
        assert_eq!(FrameType::DivinationCard.as_str(), "divination card");
        assert_eq!(FrameType::QuestItem.as_str(), "quest item");
        assert_eq!(FrameType::Prophecy.as_str(), "prophecy");
        assert_eq!(FrameType::Relic.as_str(), "relic");
    }

    #[test]
    fn serialization() -> serde_json::Result<()> {
        let u = serde_json::to_string(&FrameType::Unique)?;
        assert_eq!(u, "3");

        Ok(())
    }
}
