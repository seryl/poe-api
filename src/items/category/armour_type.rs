use std::error::Error;
use std::fmt;
use std::str::FromStr;

#[derive(Debug, PartialEq)]
pub enum ArmourType {
    Helmet,
    Gloves,
    Chest,
    Shield,
    Quiver,
    Boots,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct ArmourTypeParseError(());

impl fmt::Display for ArmourTypeParseError {
    #[allow(deprecated, deprecated_in_future)]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(self.description())
    }
}

impl Error for ArmourTypeParseError {
    #[allow(deprecated)]
    fn description(&self) -> &str {
        "unable to parse armour type"
    }
}

impl ArmourType {
    pub fn as_str(&self) -> &'static str {
        match &self {
            ArmourType::Helmet => "helmet",
            ArmourType::Gloves => "gloves",
            ArmourType::Chest => "chest",
            ArmourType::Shield => "shield",
            ArmourType::Quiver => "quiver",
            ArmourType::Boots => "boots",
        }
    }
}

impl FromStr for ArmourType {
    type Err = ArmourTypeParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let response = match s {
            "helmet" => Some(ArmourType::Helmet),
            "gloves" => Some(ArmourType::Gloves),
            "chest" => Some(ArmourType::Chest),
            "shield" => Some(ArmourType::Shield),
            "quiver" => Some(ArmourType::Quiver),
            "boots" => Some(ArmourType::Boots),
            _ => None,
        };

        match response {
            Some(response) => Ok(response),
            None => Err(ArmourTypeParseError(())),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn from_str() {
        assert_eq!(ArmourType::from_str("helmet"), Ok(ArmourType::Helmet));
        assert_eq!(ArmourType::from_str("gloves"), Ok(ArmourType::Gloves));
        assert_eq!(ArmourType::from_str("chest"), Ok(ArmourType::Chest));
        assert_eq!(ArmourType::from_str("shield"), Ok(ArmourType::Shield));
        assert_eq!(ArmourType::from_str("quiver"), Ok(ArmourType::Quiver));
        assert_eq!(ArmourType::from_str("boots"), Ok(ArmourType::Boots));
        assert_eq!(
            ArmourType::from_str("notARealValueType"),
            Err(ArmourTypeParseError(()))
        );
    }

    #[test]
    fn as_str() {
        assert_eq!(ArmourType::Helmet.as_str(), "helmet");
        assert_eq!(ArmourType::Gloves.as_str(), "gloves");
        assert_eq!(ArmourType::Chest.as_str(), "chest");
        assert_eq!(ArmourType::Shield.as_str(), "shield");
        assert_eq!(ArmourType::Quiver.as_str(), "quiver");
        assert_eq!(ArmourType::Boots.as_str(), "boots");
    }
}
