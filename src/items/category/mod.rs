pub mod accessory_type;
pub mod armour_type;
pub mod jewel_type;
pub mod weapon_type;

// use accessory_type::AccessoryType;
// use armour_type::ArmourType;
// use jewel_type::JewelType;
// use weapon_type::WeaponType;

// use serde::{Serialize, Deserialize};

/// Category
// #[derive(Serialize, Deserialize)]
pub struct Category {}

// accessories: Vec<AccessoryType>,
// armour: Vec<ArmourType>,
// jewels: Vec<JewelType>,
// weapons: Vec<WeaponType>,
