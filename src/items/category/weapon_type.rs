use std::error::Error;
use std::fmt;
use std::str::FromStr;

#[derive(Debug, PartialEq)]
pub enum WeaponType {
    OneAxe,
    TwoAxe,
    Bow,
    Claw,
    Dagger,
    Staff,
    OneSword,
    TwoSword,
    Wand,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct WeaponTypeParseError(());

impl fmt::Display for WeaponTypeParseError {
    #[allow(deprecated, deprecated_in_future)]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(self.description())
    }
}

impl Error for WeaponTypeParseError {
    #[allow(deprecated)]
    fn description(&self) -> &str {
        "unable to parse weapon type"
    }
}

impl WeaponType {
    pub fn as_str(&self) -> &'static str {
        match &self {
            WeaponType::OneAxe => "oneaxe",
            WeaponType::TwoAxe => "twoaxe",
            WeaponType::Bow => "bow",
            WeaponType::Claw => "claw",
            WeaponType::Dagger => "dagger",
            WeaponType::Staff => "staff",
            WeaponType::OneSword => "onesword",
            WeaponType::TwoSword => "twosword",
            WeaponType::Wand => "wand",
        }
    }
}

impl FromStr for WeaponType {
    type Err = WeaponTypeParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let response = match s {
            "oneaxe" => Some(WeaponType::OneAxe),
            "twoaxe" => Some(WeaponType::TwoAxe),
            "bow" => Some(WeaponType::Bow),
            "claw" => Some(WeaponType::Claw),
            "dagger" => Some(WeaponType::Dagger),
            "staff" => Some(WeaponType::Staff),
            "onesword" => Some(WeaponType::OneSword),
            "twosword" => Some(WeaponType::TwoSword),
            "wand" => Some(WeaponType::Wand),
            _ => None,
        };

        match response {
            Some(response) => Ok(response),
            None => Err(WeaponTypeParseError(())),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn from_str() {
        assert_eq!(WeaponType::from_str("oneaxe"), Ok(WeaponType::OneAxe));
        assert_eq!(WeaponType::from_str("twoaxe"), Ok(WeaponType::TwoAxe));
        assert_eq!(WeaponType::from_str("bow"), Ok(WeaponType::Bow));
        assert_eq!(WeaponType::from_str("claw"), Ok(WeaponType::Claw));
        assert_eq!(WeaponType::from_str("dagger"), Ok(WeaponType::Dagger));
        assert_eq!(WeaponType::from_str("staff"), Ok(WeaponType::Staff));
        assert_eq!(WeaponType::from_str("onesword"), Ok(WeaponType::OneSword));
        assert_eq!(WeaponType::from_str("twosword"), Ok(WeaponType::TwoSword));
        assert_eq!(
            WeaponType::from_str("notARealWeaponType"),
            Err(WeaponTypeParseError(()))
        );
    }

    #[test]
    fn as_str() {
        assert_eq!(WeaponType::OneAxe.as_str(), "oneaxe");
        assert_eq!(WeaponType::TwoAxe.as_str(), "twoaxe");
        assert_eq!(WeaponType::Bow.as_str(), "bow");
        assert_eq!(WeaponType::Claw.as_str(), "claw");
        assert_eq!(WeaponType::Dagger.as_str(), "dagger");
        assert_eq!(WeaponType::Staff.as_str(), "staff");
        assert_eq!(WeaponType::OneSword.as_str(), "onesword");
        assert_eq!(WeaponType::TwoSword.as_str(), "twosword");
    }
}
