use std::error::Error;
use std::fmt;
use std::str::FromStr;

#[derive(Debug, PartialEq)]
pub enum AccessoryType {
    Amulet,
    Belt,
    Ring,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct AccessoryTypeParseError(());

impl fmt::Display for AccessoryTypeParseError {
    #[allow(deprecated, deprecated_in_future)]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(self.description())
    }
}

impl Error for AccessoryTypeParseError {
    #[allow(deprecated)]
    fn description(&self) -> &str {
        "unable to parse accessory type"
    }
}

impl AccessoryType {
    pub fn as_str(&self) -> &'static str {
        match &self {
            AccessoryType::Amulet => "amulet",
            AccessoryType::Belt => "belt",
            AccessoryType::Ring => "ring",
        }
    }
}

impl FromStr for AccessoryType {
    type Err = AccessoryTypeParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let response = match s {
            "amulet" => Some(AccessoryType::Amulet),
            "belt" => Some(AccessoryType::Belt),
            "ring" => Some(AccessoryType::Ring),
            _ => None,
        };

        match response {
            Some(response) => Ok(response),
            None => Err(AccessoryTypeParseError(())),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn from_str() {
        assert_eq!(AccessoryType::from_str("amulet"), Ok(AccessoryType::Amulet));
        assert_eq!(AccessoryType::from_str("belt"), Ok(AccessoryType::Belt));
        assert_eq!(AccessoryType::from_str("ring"), Ok(AccessoryType::Ring));
        assert_eq!(
            AccessoryType::from_str("notARealValueType"),
            Err(AccessoryTypeParseError(()))
        );
    }

    #[test]
    fn as_str() {
        assert_eq!(AccessoryType::Amulet.as_str(), "amulet");
        assert_eq!(AccessoryType::Belt.as_str(), "belt");
        assert_eq!(AccessoryType::Ring.as_str(), "ring");
    }
}
