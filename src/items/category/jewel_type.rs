use std::error::Error;
use std::fmt;
use std::str::FromStr;

#[derive(Debug, PartialEq)]
pub enum JewelType {
    Abyss,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct JewelTypeParseError(());

impl fmt::Display for JewelTypeParseError {
    #[allow(deprecated, deprecated_in_future)]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(self.description())
    }
}

impl Error for JewelTypeParseError {
    #[allow(deprecated)]
    fn description(&self) -> &str {
        "unable to parse jewel type"
    }
}

impl JewelType {
    pub fn as_str(&self) -> &'static str {
        match &self {
            JewelType::Abyss => "abyss",
        }
    }
}

impl FromStr for JewelType {
    type Err = JewelTypeParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let response = match s {
            "abyss" => Some(JewelType::Abyss),
            _ => None,
        };

        match response {
            Some(response) => Ok(response),
            None => Err(JewelTypeParseError(())),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn from_str() {
        assert_eq!(JewelType::from_str("abyss"), Ok(JewelType::Abyss));
        assert_eq!(
            JewelType::from_str("notARealJewelType"),
            Err(JewelTypeParseError(()))
        );
    }

    #[test]
    fn as_str() {
        assert_eq!(JewelType::Abyss.as_str(), "abyss");
    }
}
