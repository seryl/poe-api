use std::fmt;

// use crate::league::LeagueType;
// use serde::{Deserialize, Serialize};

/**
 * Key	Value description	Type
 * group	group id	integer
 * attr	S, I, D, G, false (type is boolean for abyss). Stands for str, int, dex, generic?. G - white socket.	string/boolean
 * sColour	G, W, R, B, A. Stands for: green, white, red, blue, abyss (though not a colour but type).	string
 */

#[derive(Debug, PartialEq)]
pub enum SocketColor {
    Green,
    White,
    Red,
    Blue,
    Abyss,
}

impl fmt::Display for SocketColor {
    #[allow(deprecated, deprecated_in_future)]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "(SocketColor: {})", self.as_str())
    }
}

impl SocketColor {
    pub fn as_str(&self) -> &'static str {
        match &self {
            SocketColor::Green => "Green",
            SocketColor::White => "White",
            SocketColor::Red => "Red",
            SocketColor::Blue => "Blue",
            SocketColor::Abyss => "Abyss",
        }
    }
}

#[derive(Debug, PartialEq)]
pub enum SocketAttrib {
    Strength,
    Intelligence,
    Dexterity,
    Generic,
    Abyss,
}

impl fmt::Display for SocketAttrib {
    #[allow(deprecated, deprecated_in_future)]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "(SocketAttrib: {})", self.as_str())
    }
}

impl SocketAttrib {
    pub fn as_str(&self) -> &'static str {
        match &self {
            SocketAttrib::Strength => "Strength",
            SocketAttrib::Intelligence => "Intelligence",
            SocketAttrib::Dexterity => "Dexterity",
            SocketAttrib::Generic => "Generic",
            SocketAttrib::Abyss => "false",
        }
    }
}

// #[derive(Debug, PartialEq)]
// pub struct Socket {
//     group: u8,
//     attr: SocketAttrib,
//     sColor: SocketColor,
// }

// impl fmt::Display for Socket {
//     #[allow(deprecated, deprecated_in_future)]
//     fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
//         write!(f, "(Socket: {})", self.as_str())
//     }
// }
