pub mod category;
pub mod frame_type;
pub mod sockets;
pub mod value_type;

use frame_type::FrameType;

use serde::{Deserialize, Serialize};

/**
 * Key	Value description	Type	Key always present
 * additionalProperties	See properties	array[assoc]	no
 * category	maps, currency, jewels, gems, cards, flasks or category object (explained below)	string/array[assoc]	yes
 * nextLevelRequirements	See requirements	array[assoc]	no
 * properties	See below	array[assoc]	no
 * requirements	See below	array[assoc]	no
 * socketedItems	See items	array[assoc]	no
 * sockets	See below, array of sockets	array[assoc]	no
 * '^[\\s]RaceReward$'	Rare flag for race reward items. There can be few of them, so key is 'regular expression', instead of string	bool	no
 */

#[derive(Serialize, Deserialize)]
pub struct Item {
    id: String,
    name: String,
    icon: String,
    ilvl: u8,

    league: String,
    verified: bool,
    support: bool,
    identified: bool,

    #[serde(rename(serialize = "inventoryId", deserialize = "inventory_id"))]
    inventory_id: Option<String>,

    note: Option<String>,
    duplicated: Option<bool>,

    #[serde(rename(serialize = "lockedToCharacter", deserialize = "locked_to_character"))]
    locked_to_character: Option<bool>,

    #[serde(rename(serialize = "frameType", deserialize = "frame_type"))]
    frame_type: FrameType,

    elder: Option<bool>,
    shaper: Option<bool>,
    corrupted: Option<bool>,

    #[serde(rename(serialize = "artFilename", deserialize = "art_filename"))]
    art_filename: Option<String>,

    #[serde(rename(serialize = "cosmeticMods", deserialize = "cosmetic_mods"))]
    cosmetic_mods: Option<Vec<String>>,

    #[serde(rename(serialize = "craftedMods", deserialize = "crafted_mods"))]
    crafted_mods: Option<Vec<String>>,

    #[serde(rename(serialize = "enchantedMods", deserialize = "enchanted_mods"))]
    enchanted_mods: Option<Vec<String>>,

    #[serde(rename(serialize = "explicitMods", deserialize = "explicit_mods"))]
    explicit_mods: Option<Vec<String>>,

    #[serde(rename(serialize = "implicitMods", deserialize = "implicit_mods"))]
    implicit_mods: Option<Vec<String>>,

    #[serde(rename(serialize = "FlavourText", deserialize = "flavour_text"))]
    flavour_text: Option<Vec<String>>,

    #[serde(rename(serialize = "descrText", deserialize = "descr_text"))]
    descr_text: Option<String>,

    #[serde(rename(serialize = "secDescrText", deserialize = "sec_descr_text"))]
    sec_descr_text: Option<String>,

    #[serde(rename(serialize = "typeLine", deserialize = "type_line"))]
    type_line: Option<String>,

    /* League Additions */
    #[serde(rename(serialize = "talismanTier", deserialize = "talisman_tier"))]
    tailsman_tier: Option<u8>,

    #[serde(rename(serialize = "abyssJewel", deserialize = "abyss_jewel"))]
    abyss_jewel: Option<bool>,

    #[serde(rename(serialize = "isRelic", deserialize = "is_relic"))]
    is_relic: Option<bool>,

    /* Prophecy */
    #[serde(rename(serialize = "prophecyDiffText", deserialize = "prophecy_diff_text"))]
    prophecy_diff_text: Option<String>,

    #[serde(rename(serialize = "prophecyText", deserialize = "prophecy_text"))]
    prophecy_text: Option<String>,

    /* Stack Sizes */
    #[serde(rename(serialize = "stackSize", deserialize = "stack_size"))]
    stack_size: Option<u8>,

    #[serde(rename(serialize = "maxStackSize", deserialize = "max_stack_size"))]
    max_stack_size: Option<u8>,

    #[serde(rename(serialize = "utilityMods", deserialize = "utility_mods"))]
    utility_mods: Option<Vec<String>>,

    /* Positional */
    #[serde(rename(serialize = "h", deserialize = "height"))]
    height: u8,

    #[serde(rename(serialize = "w", deserialize = "width"))]
    width: u8,

    #[serde(rename(serialize = "x", deserialize = "pos_x"))]
    pos_x: u8,

    #[serde(rename(serialize = "y", deserialize = "pos_y"))]
    pos_y: u8,
}
