use std::fmt;

use crate::items::Item;

use serde::{Deserialize, Serialize};

pub struct StashUpdate {
    next_change_id: String,
    stashes: Vec<Stash>,
}

#[derive(Debug, PartialEq)]
pub enum StashTypes {
    NormalStash,
    PremiumStash,
    QuadStash,
    EssenceStash,
    CurrencyStash,
    MapStash,
    FragmentStash,
    DivinationCardStash,
}

impl fmt::Display for StashTypes {
    #[allow(deprecated, deprecated_in_future)]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "(StashType: {})", self.as_str())
    }
}

impl StashTypes {
    pub fn as_str(&self) -> &'static str {
        match &self {
            StashTypes::NormalStash => "NormalStash",
            StashTypes::PremiumStash => "PremiumStash",
            StashTypes::QuadStash => "QuadStash",
            StashTypes::EssenceStash => "EssenceStash",
            StashTypes::CurrencyStash => "CurrencyStash",
            StashTypes::MapStash => "MapStash",
            StashTypes::FragmentStash => "FragmentStash",
            StashTypes::DivinationCardStash => "DivinationCardStash",
        }
    }
}

#[derive(Serialize, Deserialize)]
pub struct Stash {
    #[serde(rename(serialize = "accountName", deserialize = "account_name"))]
    account_name: Option<String>,

    #[serde(rename(serialize = "lastCharacterName", deserialize = "last_character_name"))]
    last_character_name: Option<String>,

    id: String,
    stash: Option<String>,

    #[serde(rename(serialize = "stashType", deserialize = "stash_type"))]
    stash_type: String,

    items: Vec<Item>,

    public: bool,
    league: Option<String>,
}

// impl Description for Stash {
//     type Err = DescriptionParseError;

//     fn description_for(attribute: String) -> Result<Self, Self::Err> {
//         let response = match s {
//             "accountName" => "account name the stash linked to.",
//             "lastCharacterName" => "last character name of the player.",
//             "id" => "unique stash id.",
//             "stash" => "stash name",
//             "stashType" =>
//         };

//         match response {
//             Some(response) => Ok(response),
//             None => Err(DescriptionParseError()),
//         }
//     }
// }

//use std::fmt;

// #[derive(Debug, Clone, PartialEq, Eq)]
// pub struct DescriptionParseError(());

// impl fmt::Display for DescriptionParseError {
//     #[allow(deprecated, deprecated_in_future)]
//     fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
//         f.write_str(self.description())
//     }
// }

// impl Error for DescriptionParseError {
//     #[allow(deprecated)]
//     fn description(&self) -> &str {
//         "unable to parse job state"
//     }
// }

// trait Description {
//     type Err = DescriptionParseError;

//     fn description_for(&self, attribute: String) -> <Result<Self, Self::Err>> {};
// }

// StashTypes
